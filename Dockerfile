# Node.js resmi imajını temel alın
FROM node:20.11-alpine

# Uygulama dizinini oluştur
WORKDIR /usr/src/app

# Uygulama bağımlılıklarını kopyala
COPY package*.json ./

# Bağımlılıkları yükle
RUN npm install

# Uygulama kaynak kodunu kopyala
COPY . .

# Uygulamanın bağlanacağı porta bildir
EXPOSE 5000

# Uygulamayı çalıştır
CMD [ "node", "app.js" ]
