const axios = require('axios');

// EC2 instance'ınızın public IP adresi veya DNS adı
const EC2_URL = 'http://ec2-35-180-164-48.eu-west-3.compute.amazonaws.com:5000';

exports.handler = async (event) => {
    let path = '/api/products'; // Varsayılan path
    let method = event.httpMethod.toLowerCase(); // HTTP metodu (get, post, vb.)
    let body = event.body ? JSON.parse(event.body) : {};

    // Query parametreleri veya path parametreleri varsa path'i güncelle
    if (event.pathParameters) {
        const productId = event.pathParameters.productID;
        path += `/${productId}`;
    }

    // Axios isteğini yapılandır
    const config = {
        method: method,
        url: `${EC2_URL}${path}`,
        data: body,
    };

    try {
        const response = await axios(config);
        return {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin": "*", // Tüm kaynaklardan gelen isteklere izin ver
                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Requested-With",
                "Access-Control-Allow-Methods": "GET,POST,PUT,DELETE,OPTIONS",
                "Access-Control-Allow-Credentials": "true" // Eğer kimlik bilgileri (örneğin, çerezler, yetkilendirme başlıkları) ile istekler yapılacaksa
            },
            body: JSON.stringify(response.data),
        };
    } catch (error) {
        console.error("Error calling the API", error);
        return {
            statusCode: error.response ? error.response.status : 500,
            headers: {
                "Access-Control-Allow-Origin": "*", // CORS başlıklarını hata yanıtlarına da ekleyin
                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Requested-With",
                "Access-Control-Allow-Methods": "GET,POST,PUT,DELETE,OPTIONS",
                "Access-Control-Allow-Credentials": "true"
            },
            body: JSON.stringify(error.response ? error.response.data : { message: "Internal server error" }),
        };
    }
};
